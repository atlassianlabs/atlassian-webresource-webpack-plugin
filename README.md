This repository was moved to [https://bitbucket.org/atlassianlabs/fe-server/](https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/webresource-webpack-plugin).

See [https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/webresource-webpack-plugin](https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/webresource-webpack-plugin) for the source.

Or open issues at: [https://ecosystem.atlassian.net/jira/software/c/projects/SPFE/issues](https://ecosystem.atlassian.net/jira/software/c/projects/SPFE/issues).